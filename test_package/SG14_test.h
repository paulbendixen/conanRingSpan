#ifndef SG14_TEST
#define SG14_TEST

namespace sg14_test
{
	void ring_test();
	void static_ring_test();
	void dynamic_ring_test();
	void thread_communication_test();
	void filter_test();
}
//end SG14_TEST
#endif
