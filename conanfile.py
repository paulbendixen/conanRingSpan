from conans import ConanFile, CMake, tools
import os


class RingspanConan(ConanFile):
    name = "ring_span"
    version = "0.3"
    license = "MIT"
    url = "http://gitlab.com/paulbendixen/conanRingSpan"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    description = """
This is a package to build using the ring_span from the SG14 group.
The span is a ring buffer view of an existing memory usable in both
embedded and games as well as in generic signal processing.
"""

    def source(self):
        self.run("git clone https://github.com/WG21-SG14/SG14.git")
        # This small hack might be useful to guarantee proper /MT /MD linkage in MSVC
        # if the packaged project doesn't have variables to set it properly

    def package(self):
        self.copy("ring.h", dst="include/SG14", src="SG14/SG14")

    def package_info(self):
        self.info.header_only()
